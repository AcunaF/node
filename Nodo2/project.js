//configuracion de rutas del controlador projections

var express = require('express'); //volvemos a llamar a express 
var ProjectController = require('../Controladores/project'); //ruta del controlador que requiere project
var router = express.Router();

router.delete('/project/id', ProjectController.deleteProject);
router.get('/home', ProjectController.home); //accedo al metodo home del archivo project de la carpeta controladores
router.get('/project/id?', ProjectController.getProject); // buscar un objeto por id
router.get('/projectS', ProjectController.getProjects);
router.post('/test', ProjectController.test); //accedo al metodo test del archivo project de la carpeta controladores
router.post('/save', ProjectController.saveP); //accedo al metodo sabeP del archivo project de la carpeta controladores
router.put('/project/:id', ProjectController.updateProject);


module.exports = router;


