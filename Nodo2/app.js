//CONEXION AL SERVIDOR configracion express y bodyparser (((SEGUNDO)))

var express = require('express');//cargo modulo express
var bodyParser = require('body-parser');//cargo modulo bodyparser
var app = express();//ejecutamos express

//cargar archivos rutas
var project_routes = require('./Rutas/project');


//midlewares  una capa o metodo que se ejecuta antes de la accion del controlador, config para bodyParser
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());             // todo lo que llega lo convierte en jason, peticion por el body

//cors

//rutas se crea una ruta por cada uno de los controladores
app.use('/api', project_routes)
//REQ son los datos que yo le envio desde el cliente o la peticion que le haga y la RES es la respuesta que voy a enviar

//exportar
module.exports = app;








