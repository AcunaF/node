//un controlador es una clase que tiene una serie de metodos relacionados con el proyecto

var Project = require("../Modelo/Project");

var controller = {
    home: (req, res) => res.status(200).send({
        Project: Project,

        message: 'soy el metodo home'
    }),

    test: (req, res) => res.status(200).send({
        message: 'soy el metodo test'
    }),

    saveP: (req, res) => {

        var project = new Project();

        var params = req.body;
        project.name = params.name;
        project.description = params.description;
        project.category = params.category;
        project.year = params.year;
        project.langs = params.langs;
        project.image = null;

        project.save((err, projectStored) => {

            if (err)
                return res.status(500).send({ message: 'Error al guardar el documento' });
            if (!projectStored)
                return res.status(404).send({ message: 'No se guardo el proyecto' });

            return res.status(200).send({ project: projectStored });
        });

        return res.status(200).send({
            project: project,
            message: 'soy el metodo save project'
        });
    },
    getProject: (req, res) => {
        var projectId = req.params.id;

        if (projectId == null)
            return res.status(404).send({ message: 'el projecto no existe' });

        Project.findById(projectId, (err, project) => {

            if (err)
                return res.status(500).send({
                    message: 'error al devolver los datos del project'
                });

            if (!project)
                return res.status(404).send({
                    message: 'el projecto no existe'
                });

            return res.status(200).send({
                project
            });
        });
    },
    getProjects: (req, res) => {
        //puedo poner el año y me busca por año   
        Project.find({}).exec((err, projectS) => {


            if (err)
                return res.status(500).send({
                    mesg: 'error al devolver los datos'
                });

            if (!projectS)
                return res.status(404).send({
                    msg: ' no hay projectos que mostrar'
                });

            return res.status(200).send({ projectS });

        });
    },
    updateProject: (req, res) => {
        var projectId = req.params.id;
        var update = req.body;
        // si agrego {new:true} me devuelve el ultimo objeto agregado    
        Project.findByIdAndUpdate(projectId, update, (err, projectUpdated) => {

            if (err)
                return res.status(500).send({ msg: 'error al actulizar ' });
            if (!projectUpdated)
                return res.status(404).send({ msg: 'no existe el proyecto para actualizarlo' });

            return res.status(200).send({
                project: projectUpdated
            });
        });

    },
    deleteProject: (req, res) => {
        var projectId = req.params.id;

        Project.findByIdAndDelete(projectId, (err, projectRemoved) => {

            if (err)
                return res.status(500).send({ msg: 'error al borrar no se pudo borrar' });
            if (!projectRemoved)
                return res.status(404).send({ msg: 'No se puedo eliminar este proyecto' });

            return res.status(200).send({ project: projectRemoved });
        });
    }
};

module.exports = controller;






